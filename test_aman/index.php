<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$request = new HttpRequest();
$request->setUrl('https://www.fitmetrix.io/WebPortal/CheckInLocation/c8f8801d-d704-e511-9457-0e0c69fd6629');
$request->setMethod(HTTP_METH_GET);

$request->setQueryData(array(
  'location' => '1005%3BAspen%3BCO%3B117%20South%20Spring%20Street%3B81611%3B%28970%29%20480-5871'
));

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'Connection' => 'keep-alive',
  'cookie' => 'CheckinLocation=1005',
  'referer' => 'https://www.fitmetrix.io/WebPortal/CheckIn/c8f8801d-d704-e511-9457-0e0c69fd6629?loc=1005%3BAspen%3BCO%3B117%20South%20Spring%20Street%3B81611%3B%28970%29%20480-5871',
  'accept-encoding' => 'gzip, deflate',
  'Postman-Token' => 'b5302695-cdb2-4072-b3d7-f49e3e003b60,ae880220-7fd3-48fc-87c0-107c60cac3c2',
  'Cache-Control' => 'no-cache',
  'Accept' => '*/*',
  'User-Agent' => 'PostmanRuntime/7.11.0'
));

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}

die;
?>