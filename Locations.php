<?php
//ini_set('display_errors', 1);

ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);
//echo getcwd().'/db.php'; die;

class Locations {    
    // Declare your Custom Variables
    protected $scrapper;
    protected $apiUrl;
    protected $response;
    protected $data;
    protected $databse;
    protected $url_source;
    protected $scrapper_sources;
    public function __construct(){
        //parent::__construct();
        require_once(getcwd().'/db.php');
        $this->database = new DB_con();
        $this->url_source = 1;
        $this->scrapper_sources = array(
            '1' =>  'https://www.fitmetrix.io/WebPortal/CheckIn/c8f8801d-d704-e511-9457-0e0c69fd6629?room=0',
            '2' =>  'https://www.fitmetrix.io/WebPortal/CheckIn/33a6c534-88c0-e711-a970-8ada6ab19dbf?room=0'
        );
    }
    
    public function getCurrentLocations(){
        $response = [];
        if(!empty($this->scrapper_sources)){
            foreach($this->scrapper_sources as $key=>$scrapper){
                $this->setDataURL($scrapper);
                $this->url_source = $key;
                $this->response = $this->getDataFromSource();
                $this->data = $this->FormatHTMLResponse("option");
                //$this->printArrayObject($this->data);
                $response[] = $this->InsertLocationsArray();
                //$this->printArrayObject($response);
            }
        }
        $this->printArrayObject($response);
    }    
    
    public function getDataFromSource(){
        return file_get_contents($this->apiUrl);
    }
    
    protected function setDataURL($data_url = ''){
        $this->apiUrl = $data_url!='' ? $data_url : '';
    }
    
    protected function FormatHTMLResponse($data_container = ''){
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($this->response, 'HTML-ENTITIES', 'UTF-8'));
        $html_array = [];
        if($data_container!=''){
            $domhtml = $dom->getElementsByTagName($data_container);
            foreach ($domhtml as $child) {
                //$this->printArrayObject($item);
                if($child->getAttribute('value')){
                    $row_value = $child->getAttribute('value');
                    $row_data = explode(";",$row_value);
                    $html_array[] = array("value"=>$row_value ,"name"=>$child->textContent,"loc_id"=>$row_data[0]);
                }
            }
        }
        return $html_array;
    }
    
    protected function InsertLocationsArray(){
        if(!empty($this->data) && (is_array($this->data) || is_object($this->data)) ){
            return $this->database->addRow("current_location",$this->data,$this->url_source);
        }
    }

    protected function printArrayObject($array_object = null){
        if(!is_null($array_object)){
            echo '<pre>';
            print_r($array_object);
            echo '</pre>';
            die;
        }
    }
}

$location = new Locations;
$location->getCurrentLocations();
?>