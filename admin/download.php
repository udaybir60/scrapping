<?php
require_once('../db.php');
if(isset($_GET['zip_file']))
{
    $folder_name = '../reports/'.$_GET['zip_file'];
    $archive_file_name= $folder_name.'.zip';
    $rootPath = realpath($folder_name);
    $zip = new ZipArchive();
    $zip->open($archive_file_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
    foreach ($files as $name => $file)
    {
        if (!$file->isDir())
        {
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
            $zip->addFile($filePath, $relativePath);
        }
    }
    $zip->close();
        if(file_exists($archive_file_name))
        {
?>
<script src="vendor/jquery/jquery.min.js"></script>
<script>
    function delete_file()
    {
        return true;
    }
    $(document).ready(function(){        
        var file_url = '<?php echo base_url.'/reports'; ?>'+'/<?php echo $_GET['zip_file']; ?>'+'.zip';
        var open_window = window.open(file_url,"_self");
        window.setTimeout(function(){
            window.top.close();
        }, 500);
    });
</script>
<?php
        }
    }
?>