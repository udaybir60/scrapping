<?php
include_once('header.php');
?>
	<style>
	.btn-primary {
		    margin-top: 36%;
		}
		.col-xs-2 {
		    min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
		}
		.a {text-decoration: none;}
		th {padding: 9px;}
		td {padding: 9px;}
		
		.custom-table {
			width : 30%;
			background: #eee;
		}
	</style>
    <div id="content-wrapper">
		<link rel="stylesheet" type="text/css" href="./selector_file/multi-select.css">
      <div class="container-fluid">
		<h1>Folders to exploer and download</h1>
	<?php 
	if(isset($_POST['from_data'])) { $from_data_val = $_POST['from_data']; }else{ $from_data_val = ''; }
	if(isset($_POST['to_data'])) { $to_data_val = $_POST['to_data']; }else{ $to_data_val = ''; }
	
	?>
	<div class="container">	
	<form action='excel_file.php' method='post'>	
		  <div class="form-group row">
		  <div class="col-xs-2">
			From: <input class="form-control" name="from_data" id="ex1" type="date" value="<?php echo $from_data_val; ?>">
		  </div>
		  <div class="col-xs-2">
			To: <input class="form-control" id="ex2" name="to_data" type="date" value="<?php echo $to_data_val; ?>">
		  </div>
		  <div class="col-xs-2">
			<input class='btn btn-primary' type='submit' value='Apply'>
		  </div>
		</div>
	</form>

		 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
		<?php
    if($_SERVER['HTTP_HOST']=="127.0.0.1" || $_SERVER['HTTP_HOST']=="3.14.227.30"){
      $path = $_SERVER['DOCUMENT_ROOT'].'/scrapping/reports';
    }else{
      $path = $_SERVER['DOCUMENT_ROOT'].'/joe/reports';
    }
		$dirs = array();
		$dir = dir($path);
		$get_excel_folders = $insertdata->get_excel_folder();
		if(isset($_POST['from_data']))
		{
			$get_excel_folders = $insertdata->get_excel_folder_date_filter($_POST['from_data'],$_POST['to_data']);
			
		}
		?>
			<table class="custom-table" style="width: 85%;">
				<thead>
				<tr><th>Folder</th><th>Download</th></tr>
				</thead>
				<tbody>
			
		<?php
		$signed_date = [];
		foreach($get_excel_folders as $excel_folder) {
			if ($dh = opendir("../reports/")){
				while (($file = readdir($dh)) !== false){
					//echo $file." and ".date("m-d-Y", strtotime($excel_folder['date']))."<br>";
					//echo strpos($file,date("m-d-Y", strtotime($excel_folder['date']))); die;
					$file_data = explode("(",$file);
					if(!in_array($file,$signed_date)){
						if(isset($file_data[0]) and ($file_data[0] == date("m-d-Y", strtotime($excel_folder['date']))) and (strpos($file_data[1],"zip")===false) ){
							echo '<tr>';	
								echo '<td><a href="excel_file_sheet.php?finename='.$file.'">'.$file.'<a></td>';
								echo '<td><a href="'.base_url.'/admin/download.php?zip_file='.$file.'" target="_blank">Download Folder</a></td>';						
							echo '</tr>';							
							$signed_date[] = $file;
						}
					}
				}
			}
			closedir($dh);
		}
		
		echo '</tbody></table></div>';
	
		if(isset($_GET['zip_file']))
		{
			$folder_name = '../reports/'.$_GET['zip_file'];
			$archive_file_name= $folder_name.'.zip';
			$rootPath = realpath($folder_name);
			$zip = new ZipArchive();
			$zip->open($archive_file_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator($rootPath),
				RecursiveIteratorIterator::LEAVES_ONLY
			);
			foreach ($files as $name => $file)
			{
				if (!$file->isDir())
				{
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath) + 1);
					$zip->addFile($filePath, $relativePath);
				}
			}
			$zip->close();
				if(file_exists($archive_file_name))
				{
		?>
		<script>
			function delete_file()
			{
				return true;
			}
			$(document).ready(function(){
				var file_url = '<?php echo base_url.'/reports'; ?>'+'/<?php echo $_GET['zip_file']; ?>'+'.zip';
				var open_window = window.open(file_url,"_blank");
				open_window.delete_file();
			});
		</script>
		<?php
				}
			}
		?>
		
	  <!-- ends -->
	  <!-- jQuery -->
	 
	  <!-- Bootstrap JavaScript -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
	  <script src="./selector_file/jquery.multi-select.js"></script>
	  <script type="text/javascript">
	  // run pre selected options
	  $('#pre-selected-options').multiSelect();
	  </script>
		
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>

</body>

</html>