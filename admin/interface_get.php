<?php
	include_once('header.php');
	require('../cyclebar.php');
	require_once "../scrapping/support/web_browser.php";
	require_once "../scrapping/support/tag_filter.php";
	$web = new WebBrowser();
	
	if(isset($_POST['loc_id']))
	{
		$tableData = $insertdata->get_current_location_by_date_fileter($_POST['loc_id'],$_POST['from_date'],$_POST['to_date']);
		
	}
	$excel_name_file = date('m-d-Y(h:i-a)');
?>
    <div id="content-wrapper">

      <div class="container-fluid">
		
        <!-- Breadcrumbs-->
		 <input type="hidden" name="excel_name_file" id="excel_name_file" value="<?php echo $excel_name_file; ?>">
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>date</th>
                    <th>location</th>
                    <th>class_name</th>
                    <th>class_time</th>
                    <th>instructor</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>email</th>
                  </tr>
                </thead>
                <tbody>
					<?php foreach($tableData as $tableData_second) { ?>
					  <tr>
						<td><?php echo $tableData_second['date']; ?></td>
						<td><?php echo $tableData_second['location']; ?></td>
						<td><?php echo $tableData_second['class_name']; ?></td>
						<td><?php echo $tableData_second['class_time']; ?></td>
						<td><?php echo $tableData_second['instructor']; ?></td>
						<td><?php echo $tableData_second['first_name']; ?></td>
						<td><?php echo $tableData_second['last_name']; ?></td>
						<td><?php echo $tableData_second['email']; ?></td>
					  </tr>
					<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
