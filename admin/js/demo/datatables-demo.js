// Call the dataTables jQuery plugin
$(document).ready(function() {
	var excel_name_file = $('#excel_name_file').val();
	if(excel_name_file)
	{
	  $('#dataTable').DataTable({
		  dom: 'Bfrtip',
					buttons: [{
						extend: 'excel',
						text: 'Export',
						className: 'exportExcel',
						filename: excel_name_file,
						exportOptions: { modifier: { page: 'all'} }
					}]
	  });
	}
	// $('#dataTable').DataTable();
});
