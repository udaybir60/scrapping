<?php
	include_once('header.php');
	require('../cyclebar.php');
	require_once "../scrapping/support/web_browser.php";
	require_once "../scrapping/support/tag_filter.php";
	$web = new WebBrowser();

	$htmloptions = TagFilter::GetHTMLOptions();
	if(isset($_POST['loc_id']))
	{
		$loc_id = $_POST['loc_id'];
		foreach($loc_id as $myresult_loop) {
			$array_found_val = array_search($myresult_loop,array_column($myresult,'loc_id'));
			// $appointment_id = array();
			$url = "https://www.fitmetrix.io/WebPortal/CheckIn/c8f8801d-d704-e511-9457-0e0c69fd6629?loc=".$myresult_loop."%3BCary%3BNC%3B5022%20Arco%20Street%3B27519%3B%28919%29%20588-2124";
			$result = $web->Process($url);
			$html = TagFilter::Explode($result["body"], $htmloptions);
			$root = $html->Get();
			$rows = $root->Find(".caws1-row");
				foreach ($rows as $row)
				{
					$row_html = $row->GetOuterHTML();
					$dom = new DOMDocument();
					$dom->loadHTML(mb_convert_encoding($row_html, 'HTML-ENTITIES', 'UTF-8'));
					$items = $dom->getElementsByTagName('div');
					foreach ($items as $item) {
						if($item->getAttribute('data-id')){
							$class_location[] = array('location'=>$myresult_loop,'appointment'=>$item->getAttribute('data-id'));
						}
					}
				}
			$get_location_name[] = $myresult[$array_found_val]['name'];	
		}
		$excel_name_file = implode(',',$get_location_name);
	}else{
		$excel_name_file = date('m-d-Y(h:i-a)');
	}
	
	foreach($class_location as $class_location)
		{
			$array_found_val = array_search($class_location['location'],array_column($myresult,'loc_id'));
			$get_location_name = $myresult[$array_found_val]['name'];
			$url = 'https://www.fitmetrix.io/WebPortal/ajaxCheckinAdvClassSelection';
			$url .= '?ID=c8f8801d-d704-e511-9457-0e0c69fd6629&locationID='.$class_location['location'].'&appointmentID='.$class_location['appointment'].'&_=1556865338564';
			$base_url = get_page_data($url,$get_location_name);
			if($base_url){
			$tableData[] = $base_url;
			}
		}
	
	// echo '<pre>';
	// print_r($tableData);
	// die;
		
		// $myresult = $insertdata->get_location();
		// $class_location = $insertdata->mull_class_location($loc_id);
		
		
	// }	
?>
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>date</th>
                    <th>location</th>
                    <th>class_name</th>
                    <th>class_time</th>
                    <th>instructor</th>
                    <th>first_name</th>
                    <th>last_name</th>
                    <th>email</th>
                  </tr>
                </thead>
                <tbody>
				<?php foreach($tableData as $tableData_first) { ?>
					<?php foreach($tableData_first as $tableData_second) { ?>
					  <tr>
						<td><?php echo $tableData_second[0]; ?></td>
						<td><?php echo $tableData_second[1]; ?></td>
						<td><?php echo $tableData_second[2]; ?></td>
						<td><?php echo $tableData_second[3]; ?></td>
						<td><?php echo $tableData_second[4]; ?></td>
						<td><?php echo $tableData_second[5]; ?></td>
						<td><?php echo $tableData_second[6]; ?></td>
						<td><?php echo $tableData_second[7]; ?></td>
					  </tr>
					<?php } ?>
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
