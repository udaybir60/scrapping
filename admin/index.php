<?php
	require_once('../db.php');
	$wecome_message = 'Welcome Back!';
	if(isset($_POST['username']))
	{
		// $inputData = array('username'=>$_POST['username'],'password'=>$_POST['password']);
		$myresult = $insertdata->class_login($_POST['username'],$_POST['password']);
		if($myresult)
		{
			header("location: dashboard.php");
			die();
		}else{
			$wecome_message = 'username or password is invalid !';
		}
		
	}

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="./assets/style.css">
	<style>
		.email_class
		{
			    color: red;
				float: right;
				padding: 8px;
		}
		h2
		{
			text-align: center;
			color: #ffffff;
			font-weight: 300;
			margin: 0 0 40px;
			// font-size: 2.5rem;
		}
	</style>
</head>
<body>
	  <div class="form">
      <div class="tab-content">
	  
		<div id="login" style="display: block;">   
          <h2><?php echo $wecome_message; ?></h2>
          
          <form action="" method="post">
            <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input type="text" name='username' required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" name='password' required autocomplete="off"/>
          </div>
          <button class="button button-block"/>Log In</button>
          </form>
        </div>
		
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="https://www.jqueryscript.net/demo/jQuery-Bootstrap-Based-Toast-Notification-Plugin-toaster/jquery.toaster.js"></script>
    <script  src="./assets/index.js"></script>
	</body>
</html>
