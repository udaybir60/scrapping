<?php
include_once('header.php');
?>
	<style>
		.btn-primary {
		    margin-top: 48%;
		}
		.col-xs-2 {
		    min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
		}
		.exportExcel {
			    float: left;
				margin-left: 95%;
				width: 100%;
		}
	</style>
    <div id="content-wrapper">
		<link rel="stylesheet" type="text/css" href="./selector_file/multi-select.css">
      <div class="container-fluid">
		
		<h1>Pre-selected-options</h1>
	<?php 
	if(isset($_POST['from_date'])) { $from_data_val = $_POST['from_date']; }else{ $from_data_val = ''; }
	if(isset($_POST['to_date'])) { $to_data_val = $_POST['to_date']; }else{ $to_data_val = ''; }
	if(isset($_POST['loc_id'])) { $loc_id_val = $_POST['loc_id']; }else{ $loc_id_val = array(); }
	
	?>
	<form action='interface.php' method='post'>	
	<div class="form-group row container">
		<div class="col-xs-2">
		<label for="exampleFormControlSelect2">Multiple location select</label>
		<select multiple class="form-control" name='loc_id[]' id="exampleFormControlSelect2">
		  <?php foreach($myresult as $myresult) { ?>
			<option value='<?php echo $myresult['loc_id']; ?>' <?php if(in_array($myresult['loc_id'],$loc_id_val)) { echo 'Selected'; } ?>><?php echo $myresult['name']; ?></option>
			<?php } ?>
		</select>
	  </div>
		
	  <div class="col-xs-2">
		<label for="exampleFormControlInput1">From: </label>
		<input type="date" class="form-control" name="from_date" id="exampleFormControlInput1" value="<?php echo $from_data_val; ?>">
	  </div>
	  
	  <div class="col-xs-2">
		<label for="exampleFormControlInput1">To: </label>
		<input type="date" class="form-control" name="to_date" id="exampleFormControlInput1" value="<?php echo $to_data_val; ?>">
	  </div>
	  <div class="col-xs-2">
	  <input class='btn btn-primary' type='submit' value='Apply'>
		</div>
	</div>
	</form>
	
	
	<?php
	
	if(isset($_POST['loc_id']))
	{
		$tableData = $insertdata->get_current_location_by_date_fileter($_POST['loc_id'],$_POST['from_date'],$_POST['to_date']);
		$excel_name_file = date('m-d-Y(h:i-a)');
	?>
	<input type="hidden" name="excel_name_file" id="excel_name_file" value="<?php echo $excel_name_file; ?>">
	<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table">Membership List</i></div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Date</th>
					<th>First name</th>
                    <th>Last name</th>
                    <th>Email</th>
                    <th>Class name</th>
                    <th>Class time</th>
                    <th>Instructor</th>
					<th>Location</th>
                   
                  </tr>
                </thead>
                <tbody>
					<?php foreach($tableData as $tableData_second) { ?>
					  <tr>
						<td><?php echo $tableData_second['date']; ?></td>
						<td><?php echo $tableData_second['first_name']; ?></td>
						<td><?php echo $tableData_second['last_name']; ?></td>
						<td><?php echo $tableData_second['email']; ?></td>
						<td><?php echo $tableData_second['class_name']; ?></td>
						<td><?php echo $tableData_second['class_time']; ?></td>
						<td><?php echo $tableData_second['instructor']; ?></td>
						<td><?php echo $tableData_second['location']; ?></td>
						
					  </tr>
					<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
	<?php } ?>









	  <!-- ends -->
	  <!-- jQuery -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	  <!-- Bootstrap JavaScript -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
	  <script src="./selector_file/jquery.multi-select.js"></script>
	  <script type="text/javascript">
	  // run pre selected options
	  // $('#pre-selected-options').multiSelect();
	  // $('#multipleSelect').val(['772','1005']);
	  </script>
		
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
