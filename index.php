<?php
	ini_set("memory_limit",-1);
	ini_set('max_execution_time', 0);
	ini_set('display_errors', 1);

	require_once('db.php');
	require('cyclebar.php');
	require_once "scrapping/support/web_browser.php";
	require_once "scrapping/support/tag_filter.php";
	$web = new WebBrowser();
	$myresult = $insertdata->get_location_limit_1();
	
	$dateFormet_folder = date('m-d-Y(h:i-a)');
	$current_date = time();
	//$insertdata->insert_excel_folder(date('Y-m-d'),$dateFormet_folder);
	mkdir('./reports/'.$dateFormet_folder);	
	$htmloptions = TagFilter::GetHTMLOptions();
	// Retrieve a URL (emulating Firefox by default).
	echo '<pre>';
	$response = array();
	//print_r($myresult); die;
	foreach($myresult as $count => $myresult_loop) {		
		$url = "https://www.fitmetrix.io/WebPortal/CheckIn/c8f8801d-d704-e511-9457-0e0c69fd6629?loc=".urlencode($myresult_loop['value']);
		$result = $web->Process($url);
		// Use TagFilter to parse the content.
		$html = TagFilter::Explode($result["body"], $htmloptions);
		$root = $html->Get();
		$rows = $root->Find(".caws1-row");
		$appointment_ids = array();				

		foreach ($rows as $row){
			$row_html = $row->GetOuterHTML();
			$dom = new DOMDocument();
			$dom->loadHTML(mb_convert_encoding($row_html, 'HTML-ENTITIES', 'UTF-8'));
			$items = $dom->getElementsByTagName('div');
			foreach ($items as $item) {
				if($item->getAttribute('data-id')){
					$appointment_ids[] = array('location'=>$myresult_loop['loc_id'],'appointment'=>$item->getAttribute('data-id'));
				}
			}
		}
		
		// 
		$location_class_members = array();
		if(!empty($appointment_ids)){
			foreach($appointment_ids as $class_location){
				// $base_url = array();
				//$array_found_val = array_search($class_location['location'],array_column($myresult,'loc_id'));
				$get_location_name = $myresult_loop['name'];
				$url = 'https://www.fitmetrix.io/WebPortal/ajaxCheckinAdvClassSelection?ID=c8f8801d-d704-e511-9457-0e0c69fd6629&locationID='.$class_location['location'].'&appointmentID='.$class_location['appointment'].'&_=1556865338564';
				$class_members = get_page_data($url,$get_location_name);
				$location_class_members[] = $class_members;
			}
			
			$location_class_members = call_user_func_array('array_merge',$location_class_members);
			//print_r($location_class_members); die;
			if($location_class_members){					
				//$date_time_formet = Date('m-d-Y(h:i-a)');
				$get_location_name = $myresult_loop['name'].'-'.$dateFormet_folder;
				$file = fopen('./reports/'.$dateFormet_folder.'/'.$get_location_name.'_data.csv', 'w');
				fputcsv($file, array('Date','Location','Class name','Class time','Instructor','First name','Last Name','Email'));
				//print_r($location_class_members); die;
				foreach($location_class_members as $headlines){
					//print_r($headlines); die;
					$insertdata->insert($headlines[0],$headlines[1],$headlines[2],$headlines[3],$headlines[4],$headlines[5],$headlines[6],$headlines[7]);
					fputcsv($file,$headlines);
				}
				fclose($file);
				/*echo $txt = 'We scraped data file '.$get_location_name.' <a href="<?=base_url?>/reports/'.$dateFormet_folder.'/'.$get_location_name.'_data.csv'.'">Click Here</a> to download file';
				// $email_array = array("parasharamantyagi@gmail.com");*/
				$email_array = array("parasharamantyagi@gmail.com","uday.jbbn@gmail.com","andrewkfaulter195@gmail.com","koundal.amit007@gmail.com");
				$subject = $get_location_name.' Download CSV Data';
				$txt = 'Hello user <br> We scraped data file. <a href="'.base_url.'/reports/'.$dateFormet_folder.'/'.$get_location_name.'_data.csv'.'">Click Here</a> to download file';
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= "From: webmaster@gmail.com";
				foreach($email_array as $to) {
					mail($to,$subject,$txt,$headers);
				}
				$response['success'][] = array("location"=>$myresult_loop['name'],"result"=>"Class Members addedsuccessfullyt for all Classes");
				continue;
			}else{
				$response['failed'][] = array("location"=>$myresult_loop['name'],"result"=>"Missing Class member ID for all Classes");
				continue;
			}
		}
		// if($count == 5) { die(); }
	}
	if(isset($response['success']) and !empty($response['success'])){
		print_r($response['success']);
	}

	if(isset($response['failed']) and !empty($response['failed'])){
		print_r($response['failed']);
	}
	die;	
?>
