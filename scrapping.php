<?php
	require_once "scrapping/support/web_browser.php";
	require_once "scrapping/support/tag_filter.php";

	// Retrieve the standard HTML parsing array for later use.
	$htmloptions = TagFilter::GetHTMLOptions();

	// Retrieve a URL (emulating Firefox by default).
	$url = "https://www.fitmetrix.io/WebPortal/CheckIn/c8f8801d-d704-e511-9457-0e0c69fd6629?loc=772%3BCary%3BNC%3B5022%20Arco%20Street%3B27519%3B%28919%29%20588-2124";
	$web = new WebBrowser();
	$result = $web->Process($url);

	// Check for connectivity and response errors.
	if (!$result["success"])
	{
		echo "Error retrieving URL.  " . $result["error"] . "\n";
		exit();
	}

	if ($result["response"]["code"] != 200)
	{
		echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
		exit();
    }
    
	$baseurl = $result["url"];
    
	// Use TagFilter to parse the content.
    $html = TagFilter::Explode($result["body"], $htmloptions);

	$root = $html->Get();
    $rows = $root->Find(".caws1-row");
	foreach ($rows as $row)
	{
        $row_html = $row->GetOuterHTML();
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($row_html, 'HTML-ENTITIES', 'UTF-8'));

        $items = $dom->getElementsByTagName('div');
        foreach ($items as $item) {
            if($item->getAttribute('data-id')){
                echo '<pre>'; echo $item->getAttribute('data-id'); echo '</pre>';
            }
        }
	}
?>